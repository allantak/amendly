<div align="center">
   
   ##  🚧 Projeto Amendly - Aplicativo de comunicação ...  🚧

   <h1>
      <a href="">
         <img src="https://ik.imagekit.io/vygfbdfu5tm/Banner_rLKq2kTiV.jpg" >
      </a>

      O Amendly é um aplicativo mobile que permite uma busca de companheiro disponiveis para jogar com o mesmo gosto comum... 
   </h1>
</div>

# 🚀 Indice

- [Sobre](#-mais-sobre)
- [Funcionalidades](#-funcionalidades)
- [Tecnologias Utilizadas](#-tecnologias)
- [Desenvolvimento do ambiente](#-montagem-do-ambiente-do-desenvolvimento)
- [Como baixar o projeto](#-como-baixar-o-projeto)
- [Links Disponiveis](#-links)

## ⌨️ Desenvolvedor

- [Allan Takeuchi Bustamante](https://gitlab.com/allantak)



## 🔖&nbsp; Mais sobre

O projeto **Amendly** foi desenvolvido durante o curso de **Big Data no Agronegócio** na Fatec de Pompeia, com o intuito de colocarmos em prática todo conteúdo estudado durante o curso;


## ✨ Funcionalidades

- Cadastro, login e lougout dos usuários;
- Listagem de Comunidades de jogos;
- Interação por chat;
- Criação do proprio canal de chat;
- Listagem de Canais;

## 🔨 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Node.js](https://nodejs.org/en/)
- [React Native](https://reactnative.dev/)
- [TypeScript](https://www.typescriptlang.org/)
- [Expo](https://expo.io)
- [Firebase](https://firebase.google.com/?hl=pt-br)

## 📦 Montagem do Ambiente do Desenvolvimento:

#### Instalando NodeJS e NPM:

- Windows

```sh
    Baixar Instalador no site https://nodejs.org/
```

### Intalando o expo:

- Windows

```bash
    $ npm install --global expo-cli
```




## 🗂 Como baixar o projeto

```bash
    # Clonar o repositório
    $ git clone https://gitlab.com/allantak/amendly.git
    # Entrar no diretório
    $ cd Amendly
    # Instalar as dependências
    $ yarn install
    # Iniciar o projeto
    $ yarn start
```

---

## ⚠ Links

#### Mockup - UI Design:

 - [Link do design do app](https://www.figma.com/proto/9jUrSISkcPjcgq4Y1iffHC/Amendly?node-id=80%3A0&scaling=scale-down)

#### Video de apresentação MVP e Mockup do Amendly:

- [Link no Youtube](https://www.youtube.com/watch?v=v0ZEYq4Zy0s&feature=youtu.be)

### Video da execução do Amendly: 

- [Link no Youtube](https://www.youtube.com/watch?v=8Flf4TsYK-M&feature=youtu.be)




