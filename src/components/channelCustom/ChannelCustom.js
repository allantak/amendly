import React from 'react';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';
import {StyleSheet} from 'react-native';
import IconePerfilChannel from '../../../assets/Icons/channelPerfil.svg';
import IconEntrar from '../../../assets/Icons/LogoSign.svg';

const Total = styled.View`
    flex: 1;
    justify-content: space-between;
    flex-direction: row;
    padding: 10px;
    
`;

const TitleHead = styled.Text`
    font-size: 15px;
    font-weight:bold;
    color: #605C47;
    text-align: center;
`;

const TitleDesc = styled.Text`
    font-size: 12px;
    color: #605C47;
    margin-top: 5%;
    
`;
const AreaText = styled.View`
    flex-direction: row;
    justify-content: center;
    
`;

const AreaPerfil = styled.View`
    flex-direction: column;
    justify-content: center;
    padding-left: 10px; 
    
`;

const Separacao = styled.View`
    justify-content: center;
    

`;
const TouchIcon = styled.TouchableOpacity`
    justify-content: center;
`;

const Linhareta = styled.View`
    border-bottom-color: #E7D6D6;
    border-bottom-width: 1px;
`;




export default function ChannelCustom({TituloChannel, Name}){
    const navigation = useNavigation();

    const goTo = (screenName) =>{
        navigation.navigate(screenName);
    }
    
    return(
        <Total>
                <AreaText>
                <IconePerfilChannel width="60" height="60" />

                    <AreaPerfil>
                        <TitleHead>{TituloChannel}</TitleHead>
                        <TitleDesc>Creat for {Name}</TitleDesc>
                    </AreaPerfil>
                    
                </AreaText>
                
                <Separacao>
                    <TouchIcon>
                        <IconEntrar width="24" height="24"/>
                    </TouchIcon>
                </Separacao>
        </Total>
        
    );
    
}

const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        
    }
  });