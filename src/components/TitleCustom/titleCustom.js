import React from 'react';
import styled from 'styled-components/native';


const AreaToucha = styled.View`
    margin-bottom:20px;
    border-radius: 20px;
    padding: 15px;
    flex-direction: row;
`;
const Image = styled.Image`
    width:80px;
    height:80px;
    border-radius:20px;
`;
const TitleHead = styled.Text`
    font-size: 15px;
    font-weight:bold;
    color: #605C47;
`;
const DiscTitlee = styled.Text`
    font-size: 10px;
    color: #605C47;
`;
const AreaText = styled.View`
    padding-left: 5%;
    padding-right: 30%;
`;



export default function AbasComunity({Img, Title, DiscTitle}){
    
    return(
        <AreaToucha >
            <Image source={Img}/>
            <AreaText>
                <TitleHead>{Title}</TitleHead>
                <DiscTitlee>{DiscTitle}</DiscTitlee>
           </AreaText>
        </AreaToucha>
    );
    
}

