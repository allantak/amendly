import React from 'react';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';
import {StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


const AreaToucha = styled.TouchableOpacity`
    margin-bottom:20px;
    border-radius: 20px;
    padding: 15px;
    background-color: #FFC542;
    flex-direction: row;
`;
const Image = styled.Image`
    width:80px;
    height:80px;
    border-radius:20px;
`;
const TitleHead = styled.Text`
    font-size: 15px;
    font-weight:bold;
    color: #605C47;
`;
const DiscTitlee = styled.Text`
    font-size: 10px;
    color: #605C47;
`;
const AreaText = styled.View`
    padding-left: 5%;
    padding-right: 30%;
`;



export default function AbasComunity({Img, Title, DiscTitle, go}){
    const navigation = useNavigation();

    const goTo = (screenName) =>{
        navigation.navigate(screenName);
    }
    
    return(
        <AreaToucha onPress={()=>goTo(go)} style={styles.shadow}>
            <Image source={Img}/>
            <AreaText>
                <TitleHead>{Title}</TitleHead>
                <DiscTitlee>{DiscTitle}</DiscTitlee>
           </AreaText>
        </AreaToucha>
    );
    
}

const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
        
    }
  });
