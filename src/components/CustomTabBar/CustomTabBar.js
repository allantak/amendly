import React from 'react';
import { View } from 'react-native';
import { State } from 'react-native-gesture-handler';
import styled from 'styled-components/native';
import IconHome from '../../../assets/Icons/House.svg';
import IconPerfil from '../../../assets/Icons/Perfil.svg';

const AreaTabBar = styled.View`
    height: 55px;
    background-color: #605C47;
    flex-direction: row;
    borderTopRightRadius:15px;
    borderTopLeftRadius:15px;
    
    
`;
const TabItem = styled.TouchableOpacity`
    flex: 1;
    align-items: center;
    justify-content: center;
`;



export default function CustomTabBar({state, navigation}){
    const goTo = (screenName) =>{
        navigation.navigate(screenName);
    }
    return(
        <AreaTabBar>
            <TabItem onPress={()=>goTo('Home')}>
                <IconHome  width="24" height="24" fill={state.index==0? "#FFBC25" : "#FEF3D9" }/>
            </TabItem>
            <TabItem onPress={()=>goTo('Perfil')}>
                <IconPerfil  width="24" height="24" fill={state.index==1? "#FFBC25" : "#FEF3D9" } />
            </TabItem>
        </AreaTabBar>
    );
}