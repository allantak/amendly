import React from 'react';
import { ImageBackground, StyleSheet} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {Container,
    TextArea,
    Titulo,
    SubTitle,
    ButtonArea,
    ButtonRegist,
    TextButton,
    ButtonSing,
} from './styles';

import Logo from '../../../assets/Icons/Logo-letra.svg';
import Background from '../../../assets/Background-img/Background-iphoneX.jpg';

export default function Presentation(){
    const navigation = useNavigation();
    const navigationSignIn = () =>{
        navigation.navigate('SignIn');            

    }
    const navigationSignUp = () =>{
        navigation.navigate('SignUp');            

    }
    return(
        <Container>
                <ImageBackground source={Background} style={styles.back}>
             
                        <Logo width="100%" height="239" style={styles.logo} /> 
                   
                        

                    <TextArea>
                        <Titulo>Bem-vindo(a) ao Amendly</Titulo>
                        <SubTitle>Aonde ira encontrar companheiros disponiveis no seu momento para jogar</SubTitle>

                    </TextArea>

                    <ButtonArea>
                        <ButtonRegist onPress={navigationSignUp} style={styles.shadow}>
                                <TextButton>Registre-se</TextButton>
                        </ButtonRegist>

                        <ButtonSing onPress={navigationSignIn} style={styles.shadow}>
                                <TextButton>Entrar</TextButton>
                        </ButtonSing>
                   </ButtonArea>
                </ImageBackground>  
        </Container>
    );
}

const styles = StyleSheet.create({
    back: {
      flex: 1,
      width: "100%",
      height: "100%",
      justifyContent: "center",
      alignItems: "center",
      
    },
    logo: {
        marginTop:"30%"
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    }
  });