import React from 'react';
import styled from 'styled-components/native';


export const Container = styled.SafeAreaView`
    flex: 1;
    justify-content: center;
    align-items: center;
`;
export const ImgBack = styled.Image``;

export const Arealogo = styled.Image`
    padding: 30px;

`;

export const TextArea = styled.View`
    justify-content: center;
    align-items: center;
    padding: 15px;
    margin-top: 10px;
    
`;
export const Titulo = styled.Text`
    font-weight: bold;
    color: #FFFFFF;
    font-size: 20px;
    text-align: center;
`;
export const SubTitle = styled.Text`
    color: #FFFFFF;
    font-size: 16px;
    text-align: center;
    margin-top: 10px;
`;
export const ButtonArea = styled.View`
    width: 100%;
    padding: 20px;
    margin-top: 20px;


`;
export const ButtonRegist = styled.TouchableOpacity`
    height: 50px;
    background-color: #FFBC25;
    border-radius: 12px;
    justify-content: center;
    align-items: center;


    
`;
export const ButtonSing = styled.TouchableOpacity`
    height: 50px;
    background-color: #605C47;
    border-radius: 12px;
    justify-content: center;
    align-items: center;
    margin-top: 10px;

`;
export const TextButton =  styled.Text`
    font-weight: bold;
    color: #FFFFFF;
`;


