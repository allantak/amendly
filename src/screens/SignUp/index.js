import React, {useState}  from 'react';
import { ImageBackground, StyleSheet} from 'react-native';
import {Container,
    SetaBack,
    Title,
    AreaNickName,
    DiscText,
    AreaInfo,
    ButtonArea,
    ButtonRegist,
    TextButton
} from './styles';

import InputSign from '../../components/signInput/inputSign';
import firebase from '../../Api';
import Background from '../../../assets/Background-img/Background-iphoneX.jpg';
import { useNavigation } from '@react-navigation/native';

export default function SignUp(){
    const [nameFiel, setNameField] = useState('');
    const [emailFiel, setEmailField] = useState('');
    const [passwordField, setPasswordField] = useState('');
    const [passwordConfirmField, setPasswordConfirmField] = useState('');
    const nagation = useNavigation();
    const Register= async() =>{
        if(nameFiel != '' && emailFiel != '' && passwordField !='' && passwordConfirmField!=''){
            if(passwordField == passwordConfirmField){
                try{
                    await firebase.register(nameFiel, emailFiel, passwordField);
                    nagation.navigate('SignIn');
                }catch(error){
                    alert(error.message);
                }
            }else{
                alert("Confirmação de senha está errada!");
            }
        }else{
            alert("Preencha o campo que esta em branco!");
        }
    }
    return(
        <Container>
            <ImageBackground source={Background} style={styles.back}>
                <SetaBack/>
                
                <Title>Registre-se</Title>

                <AreaNickName>
                    <DiscText>Quer ser reconhecido como?</DiscText>
                    <InputSign placeholder="Nome de usuário" value={nameFiel} onChangeText={t=>setNameField(t)}/>
                </AreaNickName>
                    
                <AreaInfo>
                    <DiscText>Informações de conta</DiscText>
                    <InputSign placeholder="Email" value={emailFiel} onChangeText={t=>setEmailField(t)} />
                    <InputSign placeholder="Senha"value={passwordField} onChangeText={t=>setPasswordField(t)}password={true}/>
                    <InputSign placeholder="Confirmação de senha" value={passwordConfirmField} onChangeText={t=>setPasswordConfirmField(t)} password={true}/>
                </AreaInfo>

                <ButtonArea>
                    <ButtonRegist style={styles.shadow} onPress={Register}>
                        <TextButton>Criar uma conta</TextButton>
                    </ButtonRegist>
                </ButtonArea>
            </ImageBackground>
        </Container>
    );
}

const styles = StyleSheet.create({
    back: {
      flex: 1,
      width: "100%",
      height: "100%",
      justifyContent: "center",
      
      
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    }
  });