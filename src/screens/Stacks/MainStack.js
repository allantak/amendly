import React, {useState} from 'react';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();
import firebase from '../../Api';

import Preload from '../Preload';
import Presentation from '../Presentation';
import SignIn from '../SignIn';
import SignUp from '../SignUp';
import MainTab from '../Stacks/MainTab';
import RoomScreen from '../RoomScreen/RoomScreenAmong/index';
import RoomScreen1 from '../RoomScreen/RoomScreenMine/index';
import RoomScreen2 from '../RoomScreen/RoomScreendts/index';
import RoomScreen3 from '../RoomScreen/RoomScreenLoL/index';

export default function MainStack(){
    
    return(
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen name="Preload" component={Preload}/>
            <Stack.Screen name="Presentation" component={Presentation}/>
            <Stack.Screen name="SignIn" component={SignIn}/>
            <Stack.Screen name="SignUp" component={SignUp}/>
            <Stack.Screen name="MainTab" component={MainTab}/>
            <Stack.Screen name='Room' component={RoomScreen} 
                options={({ route }) => ({
                    title: route.params.thread.name
                })} 
            />
            <Stack.Screen name='RoomLol' component={RoomScreen3} 
                options={({ route }) => ({
                    title: route.params.thread.name
                })} 
            />
            <Stack.Screen name='RoomDts' component={RoomScreen2} 
                options={({ route }) => ({
                    title: route.params.thread.name
                })} 
            />
            <Stack.Screen name='RoomMine' component={RoomScreen1} 
                options={({ route }) => ({
                    title: route.params.thread.name
                })} 
            />
            
            

        </Stack.Navigator>
    );

}
    
