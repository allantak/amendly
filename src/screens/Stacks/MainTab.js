import React, { useState } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from '../Home';
import Perfil from '../Perfil';
import CustomTabBar from '../../components/CustomTabBar/CustomTabBar';
import leagueoflegendsScreen from '../Comunitys/leagueoflegendsScreen';
import amongScreen from '../Comunitys/amongScreen';
import dtsScreen from '../Comunitys/dtsScreen';
import mineScreen from '../Comunitys/mineScreen';


const Tab = createBottomTabNavigator();

export default ()=>(
    <Tab.Navigator tabBar={props=><CustomTabBar {...props}/>}>
        <Tab.Screen name="Home" component={Home}/>
        <Tab.Screen name="Perfil" component={Perfil}/>
        <Tab.Screen name="leagueoflegendsScreen" component={leagueoflegendsScreen}/>
        <Tab.Screen name="amongScreen" component={amongScreen}/>
        <Tab.Screen name="dtsScreen" component={dtsScreen}/>
        <Tab.Screen name="mineScreen" component={mineScreen}/>
        
    </Tab.Navigator>
);

/*<Tab.Screen name="Room" component={RoomScreen}  options={({ route }) => ({
    title: route.params.thread.name
})}
/>*/