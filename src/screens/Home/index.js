import React from 'react';
import AbasComunity from '../../components/AbasComunity/abasComunity';
import IconePerfil from '../../../assets/Icons/Logo-ajustado.svg';
import IconePesqui from '../../../assets/Icons/encontrar.svg';
import AmongUsIMG from '../../../assets/ImagensGames/AmongUsIMG.png';
import LolImg from '../../../assets/ImagensGames/img3.jpg';
import DSTImg from '../../../assets/ImagensGames/img2.jpg';
import MineImg from '../../../assets/ImagensGames/img1.jpg';
import { Container,
    Scroller,
    AreaSuperior,
    TitleHeader,
    TouchIcone,
    LinhaReta,
    AreaList
} from './styles';


export default ()=>{

    
    return(
        <Container>
            
                <AreaSuperior>
                    <TouchIcone>
                        <IconePerfil width="36" height="36"/>
                    </TouchIcone>
                </AreaSuperior>

                <LinhaReta/> 
                <Scroller>
                    <TitleHeader>Comunidades</TitleHeader>
                    <AreaList>
                        <AbasComunity Img={LolImg} Title="League of Legends" DiscTitle="Multiplayer online battle arena"  go="leagueoflegendsScreen" />
                        <AbasComunity Img={AmongUsIMG} Title="AmongUs" DiscTitle="O jogo segue a tripulação de uma nave espacial que precisa descobrir quem é o impostor entre eles. E ETC"  go="amongScreen"/>
                        <AbasComunity Img={DSTImg} Title="Dont Starve Together" DiscTitle="Jogo de sobrevivendia com os amigos" go="dtsScreen"/>
                        <AbasComunity Img={MineImg} Title="Minecraft" DiscTitle="Jogo de sobrevivencia, Mundo aberto"   go="mineScreen"/>
                    
                    </AreaList>
            </Scroller>
        </Container>
    );
}

