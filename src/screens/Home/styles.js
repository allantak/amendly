import React from 'react';
import styled from 'styled-components/native';


export const Container = styled.SafeAreaView`
    flex: 1;
    background-color: #FEF3D9;
`;

export const Scroller = styled.ScrollView`
    flex: 1;
`;

export const AreaSuperior = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    margin-top: 5%;
    padding: 15px;
    

`;
export const TouchIcone = styled.TouchableOpacity``;
export const TitleHeader = styled.Text`
    font-size: 24px;
    font-weight:bold;
    color: #605C47;
    padding: 24px;
`;

export const LinhaReta = styled.View`
    border-bottom-color: #E7D6D6;
    border-bottom-width: 1px;
`;

export const AreaList = styled.View`
    flex-direction: column;
    padding: 10px;
`;