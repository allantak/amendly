import React , { useState, useEffect } from 'react';
import {StyleSheet,TouchableHighlight, FlatList, TouchableOpacity, Alert} from 'react-native';
import { Overlay, Text } from 'react-native-elements';
import { List, Divider } from 'react-native-paper';
import firebase from '../../../Api';
import Input from '../../../components/signInput/inputOverlay';
import IconeSetaBack from '../../../../assets/Icons/costas.svg';
import { useNavigation } from '@react-navigation/native';
import TitleCustom from '../../../components/TitleCustom/titleCustom';
import DtsIMG from '../../../../assets/ImagensGames/img2.jpg';
import IconceMais from '../../../../assets/Icons/mais.svg';
import { Container,
    Scroller,
    LinhaReta,
    AreaSuperior,
    TouchIcone,
    AreaList,
    AreaTitle,
    TitleHeader,
    RenButton,
    OverAreaInfo,
    OverText,
    OverAreaText,
    OverButtonA,
    OverButtonArea,
    OverHeader,
    TextButton,
    OverObjt
} from './styles';



export default ()=>{
    const navigation = useNavigation();
    const [visible, setVisible] = useState(false);
    const [value, setValue] = useState("");
    const [threads, setThreads] = useState([]);
    const userr = firebase.currentUser();

    useEffect(()=>{
        const unsubscribe = firebase.firestore()
        .collection('THREADS2')
        .onSnapshot(querySnapshot => {
            const threads = querySnapshot.docs.map(documentSnapshot =>{
                return{
                    _id: documentSnapshot.id,
                    name: '',
                    ...documentSnapshot.data()
                };
            });
            setThreads(threads);
        });
        return () => unsubscribe();
        },[]);
    
   
    
    const toggleOverlay = () => {
        setVisible(!visible);
        var user = firebase.currentUser();
        console.log(user.uid);
        console.log(user.email);
        
    };

    const handleButtonPress = () =>{
        if(value.length<=27){
            firebase.firestore()
            .collection('THREADS2')
            .add({
                name:value,
                username: userr.displayName,
                latestMessage: {
                    text: `Entrou na sala ${value}.`,
                    createdAt: new Date().getTime()
                  }
            })
            .then(docRef => {
                docRef.collection('MESSAGES2').add({
                  text: `Entrou na sala ${value}.`,
                  createdAt: new Date().getTime(),
                  system: true
                });
                setVisible(false)
            });
        }else{
            alert("Nome já existente ou ultrapassagem de caracteres");
        }
        
    }

    const goBack = () =>{
        navigation.goBack();
    }

    function deleteAddress({thread}) {
        Alert.alert(
          'Deletar seu canal',
          'Tem certeza que quer deletar ?',
          [
            {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            {text: 'OK', onPress: () =>deleteScreen({thread})},
          ],
          { cancelable: false }
        )
      }

    const deleteScreen= ({thread})=>{
        if(thread.username == userr.displayName){
            firebase.firestore()
            .collection('THREADS2')
            .doc(thread._id)
            .delete()
            .then(function() {
                console.log("Document successfully deleted!");
            }).catch(function(error) {
                console.error("Error removing document: ", error);
            });
        }else{
            Alert.alert('Delete incorreto',
            'Esse canal não te pertence'),
            [
                {text: 'Ok', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              ],
              { cancelable: false }
        }
        
    }

    return(
        <Container>

            <TouchableHighlight activeOpacity={0.5} style={styles.TouchableOpacityStyle} >
                <RenButton style={styles.shadow, styles.FloatingButtonStyle} onPress={toggleOverlay}  >
                    <IconceMais  width="24" height="24" />
                </RenButton>
            </TouchableHighlight>

            <Overlay onBackdropPress={() => console.log('Backdrop pressed')}
             isVisible={visible} 
             onBackdropPress={toggleOverlay} 
             overlayStyle={styles.AreaOver, styles.AreaTamanhoOver}
             onRequestClose={toggleOverlay}
            >    
                <OverObjt>
                    <OverHeader>
                        <TouchIcone>
                            <IconeSetaBack  width="12" height="12"/>
                        </TouchIcone>
                    </OverHeader>

                    <OverAreaText>
                        <OverText>Criação de sala!</OverText>     
                    </OverAreaText> 

                    <OverAreaInfo>
                        <Input placeholder="Nome sala: " value={value} onChangeText={t=>setValue(t)}/> 
                    </OverAreaInfo>

                    <OverButtonArea>
                        <OverButtonA disabled={value.length === 0} onPress={() => handleButtonPress()}>
                            <TextButton>Criar</TextButton>    
                        </OverButtonA>    
                    </OverButtonArea>

                </OverObjt>
                        
            </Overlay>

                <AreaSuperior>
                    <TouchIcone onPress={goBack}>
                        <IconeSetaBack  width="24" height="24"/>
                    </TouchIcone>
                </AreaSuperior>

                <LinhaReta/>

            
                <AreaTitle>
                    <TitleCustom Img={DtsIMG} Title="Dont Starve Together" DiscTitle="Jogo de sobrevivendia com os amigos"/>
                </AreaTitle>

                <TitleHeader>Canais</TitleHeader>
                
                <AreaList>
                        <FlatList
                            data={threads}
                            keyExtractor={item => item._id}
                            ItemSeparatorComponent={() => <Divider/>}
                            renderItem={({item})=>(
                                <TouchableOpacity
                                onPress={() => navigation.navigate('RoomDts', { thread: item })}
                                onLongPress={() => deleteAddress({ thread: item })}
                                >
                                    <List.Item
                                        title={item.name}
                                        description={"created by " + item.username}
                                        titleNumberOfLines={1}
                                        titleStyle={styles.listTitle}
                                        descriptionStyle={styles.listDescription}
                                        descriptionNumberOfLines={1}
                                    >
                                    </List.Item>
                                </TouchableOpacity>
                            )
                        } 
                        />
                    
                    
                    
                </AreaList>

            
            
        </Container>
    );
}
//27 no maximo
const styles = StyleSheet.create({
    TouchableOpacityStyle: {
        zIndex: 1,
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 30,
        bottom: 30,
      },
    shadow : {
        resizeMode: 'contain',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },

    FloatingButtonStyle: {
        resizeMode: 'contain'
    },

    AreaOver: {
        backgroundColor: "#605C47",
        
    },
    AreaTamanhoOver: {
        width:"80%",
        backgroundColor: "#605C47",
        borderRadius: 24
        
        
        
    }
}
)