import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    flex: 1;
    background-color: #FEF3D9;
    justify-content: center;
    
`;

export const Scroller = styled.ScrollView`
    flex: 1;
    padding: 10px;
    flex-direction: column;
`;

export const LinhaReta = styled.View`
    border-bottom-color: #E7D6D6;
    border-bottom-width: 1px;
`;

export const AreaSuperior = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    margin-top: 5%;
    padding: 15px;
`;

export const TouchIcone = styled.TouchableOpacity``;

export const AreaList = styled.View`
    flex: 1;
    flex-direction: column;
    padding: 5px;
`;

export const AreaTitle = styled.View`
    justify-content: center;
    margin-top: 5%;
    padding-left: 10px;
`;
export const AreaButton = styled.View`
    align-self: flex-end;
    padding:10px;
`;
export const RenButton = styled.TouchableOpacity`
    justify-content: center;
    align-items: center;
    background-color: #0D2DB6;
    border-radius:60px;
    width: 70px;
    height: 70px;


`;

export const TitleHeader = styled.Text`
    font-size: 24px;
    font-weight:bold;
    color: #605C47;
    padding-left: 15px;

    
`;

///////////////////////////////////////////////////////////////////////////////////////////////////////
//////// Overlay ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

export const OverObjt = styled.View`
    background-color: #605C47;
    
`;
export const OverHeader = styled.View`
    align-self: flex-end;
    margin-right: 5%;
    margin-top: 5%;
    
`;
export const OverButtonArea = styled.View`
    padding: 20px;
    justify-content: center;
    align-items: center;

`;
export const OverButtonA = styled.TouchableOpacity`
    height: 50px;
    background-color: #FFBC25;
    width: 100%;
    border-radius: 18px;
    justify-content: center;
    align-items: center;
`;
export const OverAreaText = styled.View`
    
    padding: 5px;
    margin-top: 5px;
`;
export const OverText = styled.Text`
    font-weight: bold;
    color: #FFFFFF;
    font-size: 15px;
    padding-left: 10%;
   
`;

export const OverAreaInfo = styled.View`
   
    padding-top: 5px;
    padding-left: 10%;
    padding-right: 10%;
    padding-bottom: 10px;
     
`;

export const TextButton = styled.Text`
    font-weight: bold;
    color: #FFFFFF;
`;









