import React, { useEffect } from 'react';
import { ImageBackground, StyleSheet} from 'react-native';
import {Container, LoadingIcon} from './styles';
import Logo from '../../../assets/Icons/Logo-icone.svg';
import Background from '../../../assets/Background-img/Background-iphoneX.jpg';
import AsyncStorage from '@react-native-community/async-storage';
import { useNavigation } from '@react-navigation/native';


export default function Preload(){
    const navigation = useNavigation();
    useEffect(()=>{
        const checktoken = async () => {
            const token = await AsyncStorage.getItem('token');
            if(token !== null){
                
            }
            else{
                navigation.navigate('Presentation');            
            }
        }
        checktoken();    
    },[]);
    return(
            <Container>
                <ImageBackground source={Background} style={styles.back}>
                    <Logo width="100%" height="200"/>  
                    <LoadingIcon size="large" color="#FFFF"/> 
                </ImageBackground>  
            </Container>
        
    );
}

const styles = StyleSheet.create({
    back: {
      flex: 1,
      width: "100%",
      height: "100%",
      justifyContent: "center",
      alignItems: "center",
      
    },
  });