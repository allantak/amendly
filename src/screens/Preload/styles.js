import React from 'react';
import styled from 'styled-components/native';
import Background from '../../../assets/Background-img/Background-iphoneX.jpg';

export const Container = styled.SafeAreaView`
    flex: 1;
    justify-content: center;
    align-items: center;
`;
export const ImgBack = styled.Image``;

export const LoadingIcon = styled.ActivityIndicator`
    margin-top: 10px;
`;






