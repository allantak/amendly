import React from 'react';
import styled from 'styled-components/native';


export const Container = styled.SafeAreaView`
    flex: 1;
    background-color: #FEF3D9;
    align-items: center;
`;
export const Banner = styled.Image`
    width:100%;
    height:200px;
    borderBottomRightRadius:15px;
    borderBottomLeftRadius:15px;
`;
export const TextName = styled.Text`
    font-size: 15px;
    font-weight:bold;
    padding-top: 65px;
    color: #66614C;
    
    
`;
export const Risco = styled.View`
    border-bottom-color: #E7D6D6;
    border-bottom-width: 1px;
`;
export const AreaButton = styled.View`
    flex-direction: column-reverse;
    align-items: center;
    padding: 15px;
    flex: 2;
`;
export const LogoutButton = styled.TouchableOpacity`
    height: 50px;
    background-color: #605C47;
    border-radius: 15px;
    justify-content: center;
    align-items: center;
    width: 100px;
`;

export const IconePerfil = styled.Image`
    border-radius:15px;
`

export const TextButton = styled.Text`
    font-weight: bold;
    color: #FFFFFF;
`;

export const ImgPerfil = styled.Image`
    position: absolute;
    width: 117px;
    height: 117px;
    top: 144px;
`;