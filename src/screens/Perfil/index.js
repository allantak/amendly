import React, {useState, useEffect}from 'react';
import {Text, StyleSheet, View} from 'react-native';
import ImgBackground from '../../../assets/Background-img/Background-iphoneX.jpg';
import ImgPerfilChannel from '../../../assets/Icons/IMGPerfilAmendly.png';
import firebase from '../../Api';
import { Container, Banner, TextName, Risco,AreaButton, LogoutButton, IconePerfil,TextButton, ImgPerfil} from './styles';
import { useNavigation } from '@react-navigation/native';
export default ()=>{
    const navigation = useNavigation();
    const [nameFiel, setNameField] = useState(null);
    const user = firebase.currentUser();
    useEffect(()=>{
        const checkuser = () => {
            if(user){
                setNameField(user.displayName);
            }
            else{
                console.log('error');           
            }
        }
        checkuser();    
    },[]);
    const handleSignClick = () => {
            const logout = firebase.logout();
            if(logout){
                navigation.navigate('SignIn');
            }else{
                console.log("deu errado")
            }
        
    }

    return(
        <Container>

            <Banner source={ImgBackground} />
            <ImgPerfil  source={ImgPerfilChannel} width="100" height="100" />
            <TextName>{nameFiel}</TextName>
            <Risco/>

            <AreaButton>
                <LogoutButton style={styles.shadowdoImg} onPress={handleSignClick}>
                    <TextButton >Logout</TextButton>
                </LogoutButton>
            </AreaButton>
            
        </Container>
    );
}

const styles = StyleSheet.create({
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        
    },
    shadowdoImg: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    }
})