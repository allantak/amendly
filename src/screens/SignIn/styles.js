import React from 'react';
import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const SetaBack = styled.TouchableOpacity``;
export const Title = styled.Text`
    color: #FFFFFF;
    font-size: 20px;
    text-align: center;
    font-weight: bold;

`;
export const AreaNickName = styled.View`
    justify-content: center;
    padding: 10%;   
    
`;
export const DiscText = styled.Text`
    color: #FEF3D9;
    font-size: 16px;
    font-weight: bold;
    padding-left: 5%;
    margin-bottom: 2%;
`;
export const AreaInfo = styled.View`
    justify-content: center;
    padding: 10%;
     
`;
export const ButtonArea = styled.View`

    width: 100%;
    padding: 20px;
    margin-top: 20px;
`;
export const ButtonRegist = styled.TouchableOpacity`
    height: 50px;
    background-color: #FFBC25;
    border-radius: 12px;
    justify-content: center;
    align-items: center;
`;
export const TextButton = styled.Text`
    font-weight: bold;
    color: #FFFFFF;
`;