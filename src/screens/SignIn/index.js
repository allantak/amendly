import React, {useState} from 'react';
import { ImageBackground, StyleSheet,  Alert} from 'react-native';
import {Container,
    SetaBack,
    Title,
    AreaNickName,
    DiscText,
    AreaInfo,
    ButtonArea,
    ButtonRegist,
    TextButton
} from './styles';

import InputSign from '../../components/signInput/inputSign'
import Logo from '../../../assets/Icons/Logo-icone.svg';
import Background from '../../../assets/Background-img/Background-iphoneX.jpg';
import firebase from '../../Api';
import { useNavigation } from '@react-navigation/native';


export default function SignIn(){
    const navigation = useNavigation();
    const [emailFiel, setEmailField] = useState();
    const [passwordField, setPasswordField] = useState();

    const handleSignClick = async ()=> {
        if(emailFiel != '' && passwordField !=''){
            try{
                await firebase.login(emailFiel,passwordField);
                const user = firebase.currentUser();
                if(user){
                    navigation.navigate('MainTab');
                    
                    /*if (user != null) {
                        user.providerData.forEach(function (profile) {
                          console.log("Sign-in provider: " + profile.providerId);
                          console.log("  Provider-specific UID: " + profile.uid);
                          console.log("  Name: " + profile.displayName);
                          console.log("  Email: " + profile.email);
                          console.log("  Photo URL: " + profile.photoURL);
                        });
                      }*/
                }
            }catch(error){
                alert("Email/Senha está incorreto");
            }
        }else{
            alert("Preencha as lacunas em branco")
        }  
    }
    
    return(
        <Container>

            <ImageBackground source={Background} style={styles.back}>
                <SetaBack/>
                
                <Logo width="100%" height="239"/> 
                
                <Title>Boas-vindas novamente!</Title>

                <AreaNickName>
                    <InputSign placeholder="Email" value={emailFiel} onChangeText={t=>setEmailField(t)}/>
                    <InputSign placeholder="Senha" value={passwordField} onChangeText={t=>setPasswordField(t)} password={true}/>
                </AreaNickName>
                    

                <ButtonArea>
                    <ButtonRegist onPress={handleSignClick} style={styles.shadow}>
                        <TextButton>Entrar</TextButton>
                    </ButtonRegist>
                </ButtonArea>
            </ImageBackground>
        </Container>
    );
}

const styles = StyleSheet.create({
    back: {
      flex: 1,
      width: "100%",
      height: "100%",
      justifyContent: "center",
      
      
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    }
  });