
//import firebase from 'react-native-firebase';
import React,{useState} from 'react';
import firebase from 'firebase/app'
import 'firebase/firebase-firestore'
import 'firebase/firebase-auth'
import { LogBox } from 'react-native';
import _ from 'lodash';

LogBox.ignoreLogs(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

import { useNavigation } from '@react-navigation/native';

//export default {
  //  checkToken:async () => {

    //},
    //signIn: async (email, password) => {
        
    //},
    //signUp: async () => {
        
    //}

//}

const firebaseConfig = {
    apiKey: "AIzaSyALTrqVNcuEwg6W45ro6vSgSLkWx36OGz8",
    authDomain: "amendly-d30c9.firebaseapp.com",
    databaseURL: "https://amendly-d30c9.firebaseio.com",
    projectId: "amendly-d30c9",
    storageBucket: "amendly-d30c9.appspot.com",
    messagingSenderId: "112684656268",
    appId: "1:112684656268:web:8a2ef63540617f0ba07da9",
    measurementId: "G-ETRK963GNE"
  };
class Firebase {
    
    constructor(){
        firebase.initializeApp(firebaseConfig);
        this.auth = firebase.auth();
        this.db = firebase.firestore();
    }
    async login(email, password){
        await this.auth.signInWithEmailAndPassword(email, password);
            return console.log('User signed out!');
            
    }
    logout(){
        return this.auth.signOut().then(() => console.log('User signed out!'));
    }
    async register(name, email, password){
        await this.auth.createUserWithEmailAndPassword(email, password)
            return this.auth.currentUser.updateProfile({
                displayName:name
            })
        }
    componentDidMount() {
        this.auth.currentUser.getIdToken();
        }
    currentUser(){
        return this.auth.currentUser;
    } 
    
    firestore(){
        return this.db;
    }
        
    }
    


export default new Firebase;