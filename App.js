import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import MainStack from './src/screens/Stacks/MainStack';


export default function Main() {
  return(
    <NavigationContainer>
      <MainStack/>
    </NavigationContainer>

  );
}

